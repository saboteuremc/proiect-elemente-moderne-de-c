#include "SaboteurV1.h"
 std::string LogTypeToString(Saboteur::Type type)
{
	switch (type)
	{
	case Saboteur::Type::Info:
		return "Info";
	case Saboteur::Type::Warning:
		return "Warning";

	default:
		return "";
	}
}

Saboteur::Saboteur(std::ostream & os) :
	os(os)
{
}

void Saboteur::log( std::string message)
{
	os << message << std::endl;
}

std::string Saboteur::showMessage(Saboteur::Actions actionType,Saboteur::Type type)
{


	std::string myString = "";
	switch (actionType)
	{
	case  Actions::boardCreated:
		{
			myString = "["+LogTypeToString(type)+ "]" +"Board has been created";
			break;
		}
	case Actions::badPositionChosen:
		{
			myString = "[" +LogTypeToString(type)+ "]"+"Current player has chosen an inacessible position";
			break;
		}
	case Actions::boardPositionChosen:
		{
		myString = "[" +LogTypeToString(type)+ "]"+"Current player has chosen this position on the board:";
		break;
		}
	
	case Actions::playerAction:
	{
		myString = "[" +LogTypeToString(type)+ "]"+"Current player has chosen this action";
		break;
	}
	
	case Actions::playerChoose:
	{
		myString = "[" +LogTypeToString(type)+ "]"+"Current player has chosen a card";
		break;
	}
	case Actions::currentPlayer:
	{
		myString = "[" +LogTypeToString(type)+ "]"+"Current player is: ";
		break;
	}
	case Actions::playersReady:
	{
		myString = "[" +LogTypeToString(type)+ "]"+"Player is ready to start the game!";
		break;
	}
	case Actions::roundStarted:
	{
		myString = "[" +LogTypeToString(type)+ "]"+"Round number: ";
		break;
	}
	case Actions::roundEnded:
	{
		myString = "[" +LogTypeToString(type)+ "]" + "Round has ended";
		break;
	}
	case Actions::diggersWon:
	{
		myString = "[" + LogTypeToString(type) + "]" + "DIGGERS WON";
		break;
	}
	case Actions::saboteursWon:
	{
		myString = "[" + LogTypeToString(type) + "]" + "SABOTEUR WON";
		break;
	}
	case Actions::goldDistribution:
	{
		myString = "[" + LogTypeToString(type) + "]" + "Gold is being distributed to the winners";
		break;
	}

	}
	return myString;
}

//std::string Saboteur::boardDraw()
//{
//	std::string myString = "board created";
//	return myString;
//}
//
//std::string Saboteur::initDwarfDeck()
//{
//	std::string myString = " Init dwarf deck created";
//	return myString;
//}
