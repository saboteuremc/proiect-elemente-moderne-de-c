#include "Game.h"

void Game::initPathCardDeck()
{
	int index = 0;
	initPathCards(index);
	initActionCards(index);
}

void Game::initPathCards(int& index)
{
	for (int i = 0; i < 5; i++)
	{
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::SWEC);
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::NSWEC);
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::NSEC);
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::NWC);
	}

	for (int i = 0; i < 4; i++)
	{
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::WEC);
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::SWC);
	}

	for (int i = 0; i < 3; i++)
	{
		pathCardDeck[index++] = new PathCard(Enums::PathCardType::NSC);
	}

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::WC);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NW);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NWE);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::SW);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NC);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NSE);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NS);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::NSWE);

	pathCardDeck[index++] = new PathCard(Enums::PathCardType::WE);

}

void Game::initActionCards(int& index)
{
	for (int i = 0; i < 2; i++)
	{
		pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairPickaxe);
		pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairCart);
		pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairLamp);
	}

	for (int i = 0; i < 3; i++)
	{
		pathCardDeck[index++] = new BrokenToolCard(Enums::ActionCardType::BrokenPickaxe);
		pathCardDeck[index++] = new BrokenToolCard(Enums::ActionCardType::BrokenCart);
		pathCardDeck[index++] = new BrokenToolCard(Enums::ActionCardType::BrokenLamp);
	}

	for (int i = 0; i < 6; i++)
	{
		pathCardDeck[index++] = new MapCard();
	}

	for (int i = 0; i < 3; i++)
	{
		pathCardDeck[index++] = new RockFallCard();
	}

	pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairPickaxeAndCart);
	pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairLampAndCart);
	pathCardDeck[index++] = new RepairTool(Enums::ActionCardType::RepairPickaxeAndLamp);
}

void Game::initGoldCardDeck()
{
	int goldCardNumbers = 17;
	for (int i = 0; i < goldCardNumbers; i++)
	{
		goldCardDeck.push_back(1);
	}

	goldCardNumbers = 7;
	for (int i = 0; i < goldCardNumbers; i++)
	{
		goldCardDeck.push_back(2);
	}

	goldCardNumbers = 4;
	for (int i = 0; i < goldCardNumbers; i++)
	{
		goldCardDeck.push_back(3);
	}
}

void Game::initDwarfCardDeck(const int& numberOfPlayers, std::vector<bool>& dwarfCardDeck)
{
	switch (numberOfPlayers)
	{
	case 3:
		dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 3; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 4:
		dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 4; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 5:
		for (int i = 0; i < 2; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 4; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 6:
		for (int i = 0; i < 2; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 5; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 7:
		for (int i = 0; i < 3; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 5; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 8:
		for (int i = 0; i < 3; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 6; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 9:
		for (int i = 0; i < 3; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 7; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	case 10:
		for (int i = 0; i < 4; i++)
			dwarfCardDeck.push_back(SABOTEUR);
		for (int i = 0; i < 7; i++)
			dwarfCardDeck.push_back(GOLD_MINER);
		break;
	default:
		break;
	}
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(dwarfCardDeck.begin(), dwarfCardDeck.end(), generator);
}

void Game::distributePathCards(const int & numberOfPlayers, Player * player, int& index)
{
	int numberOfCards;

	if (numberOfPlayers >= 3 && numberOfPlayers <= 5)
	{
		numberOfCards = 6;
	}
	else if (numberOfPlayers >= 6 && numberOfPlayers <= 7)
	{
		numberOfCards = 5;
	}
	else if (numberOfPlayers >= 8 && numberOfPlayers <= 10)
	{
		numberOfCards = 4;
	}

	player->getCards().setSize(numberOfCards);
	for (int i = 0; i < numberOfCards; i++)
	{
		player->getCards()[i] = std::move(pathCardDeck[index++]);
		pathCardDeck[i] = std::nullopt;
	}
}

Game::Game()
{
	this->goldCardDeck.resize(28);
	this->pathCardDeck.setSize(71);
}

void Game::start()
{
	initPathCardDeck();
	initGoldCardDeck();

	int numberOfPlayers;
	bool correctNumberOfPlayersWasChoosen = false;

	while (!correctNumberOfPlayersWasChoosen)
	{
		std::cout << "Please enter the number of players (between 3-10): ";
		std::cin >> numberOfPlayers;

		if (numberOfPlayers >= 3 && numberOfPlayers <= 10)
		{
			correctNumberOfPlayersWasChoosen = true;
		}
		else 
		{
			system("cls");
		}
	}

	/* Read each player */
	for (int i = 0; i < numberOfPlayers; i++)
	{
		std::cout << "Player " << i + 1 << ": ";
		Player* newPlayer = new Player();
		std::cin >> *newPlayer;

		players.push_back(newPlayer);
	}

	/* Shuffle the Dwarf Card Deck and distribute one of it to each player */
	initDwarfCardDeck(numberOfPlayers, dwarfCardDeck);
	int count = 0;
	for (auto player : players)
	{
		player->setType(dwarfCardDeck[count]);
		count++;
	}

	/* Shuffle the Path Card Deck and distribute to each player */
	pathCardDeck.shuffle();
	int index = 0;
	for (auto player : players)
	{
		distributePathCards(numberOfPlayers, player, index);
	}

	gameLoop();
}

void Game::gameLoop()
{
	//std::vector<std::unique_ptr<Player>>::iterator it = std::min(players.begin(), players.end());
	int min = INT_MAX, index = 0;
	for (int i = 0; i < players.size(); i++)
	{
		if (min > players[i]->getAge())
		{
			index = i;
			min = players[i]->getAge();
		}
	}
	
	Player* currentPlayer = players[index];
	while (true)
	{
		Graphic::draw(board);
		
		std::cout << std::endl;
		std::cout << *currentPlayer << std::endl;

		int choice;
		std::cout << "Options:" << std::endl;
		std::cout << "1. Select card from hand" << std::endl;
		std::cout << "2. Pass" << std::endl;
		std::cin >> choice;

		PathCard* pathCard;
		ActionCard* actionCard;

		RepairTool* repairCard;
		BrokenToolCard* brokenCard;
		MapCard* mapCard;
		RockFallCard* rockFallCard;

		Board::Position position;
		int x, y;

		switch (choice)
		{
		case 1:
			std::cout << "Card:";
			std::cin >> choice;
			std::cout << std::endl;

			pathCard = dynamic_cast<PathCard*>(currentPlayer->getCards()[choice].value());
			actionCard = dynamic_cast<ActionCard*>(currentPlayer->getCards()[choice].value());

			if (pathCard != nullptr)
			{
				std::cout << "Place card at: ";
				std::cin >> x >> y;
				
				board[Board::Position(x, y)] = pathCard->getType();
			}
			else if (actionCard != nullptr)
			{
				repairCard = dynamic_cast<RepairTool*>(actionCard);
				brokenCard = dynamic_cast<BrokenToolCard*>(actionCard);
				mapCard = dynamic_cast<MapCard*>(actionCard);
				rockFallCard = dynamic_cast<RockFallCard*>(actionCard);

				if (repairCard != nullptr)
				{
					repairCard->executeAction(currentPlayer);
				}
				else if (brokenCard != nullptr)
				{
					brokenCard->executeAction(currentPlayer);
				}
				else if (mapCard != nullptr)
				{
					mapCard->executeAction(currentPlayer);
				}
				else if (rockFallCard != nullptr)
				{
					rockFallCard->executeAction(currentPlayer);
				}
			}
			break;
		case 2:
			break;
		}
		index = (index + 1) % players.size();
		currentPlayer = players[index];
	}
}
