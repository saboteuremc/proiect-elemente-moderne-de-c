#pragma once
#include <iostream>
#include <string>
//
//#ifdef LOGGING_EXPORTS
//	#define LOGGING_API __declspec(dllexport)
//#else
//	#define LOGGING_API __declspec(dllimport)
//#endif
class __declspec(dllexport) Saboteur
{
public : 
	enum class Type
	{
		Info,
		Warning
	};
	enum class Actions
	{
		boardCreated,
		cardsShuffled,
		playersReady,
		roundStarted,
		playerChoose,
		playerAction,
		boardPositionChosen,
		badPositionChosen,
		currentPlayer,
		roundEnded,
		diggersWon,
		saboteursWon,
		goldDistribution
	};

public:

	Saboteur(std::ostream& os);
	void log(std::string message);
	std::string showMessage(Saboteur::Actions actionType,Saboteur::Type type);
	


private:
	std::ostream& os;
};
