#pragma once

#include <vector>
#include <memory>
#include <algorithm>

#include <iterator>
#include <random>

#include "Board.h"
#include "Player.h"
#include "CardDeck.h"
#include "Graphic.h"

#include "PathCard.h"
#include "MapCard.h"
#include "RepairTool.h"
#include "BrokenToolCard.h"
#include "RockFallCard.h"

class Game
{
private:
	const bool GOLD_MINER = true;
	const bool SABOTEUR = false;
public:
	Game();

	void start();
private:
	void initPathCardDeck();
	void initPathCards(int& index);
	void initActionCards(int& index);

	void initGoldCardDeck();
	void initDwarfCardDeck(const int& numberOfPlayers, std::vector<bool>& dwarfCardDeck);

	void distributePathCards(const int& numberOfPlayers, Player* player, int& index);

	void gameLoop();

private:
	Board board;
	std::vector<Player*> players;

	CardDeck pathCardDeck;
	CardDeck discardDeck;

	std::vector<bool> dwarfCardDeck;
	std::vector<uint8_t> goldCardDeck;
};

