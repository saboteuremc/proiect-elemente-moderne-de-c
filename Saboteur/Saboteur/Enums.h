#pragma once
class Enums
{
public:
	enum class ActionCardType
	{
		Undefined,

		RepairPickaxe,
		RepairCart,
		RepairLamp,
		
		RepairPickaxeAndLamp,
		RepairPickaxeAndCart,
		RepairLampAndCart,

		BrokenPickaxe,
		BrokenCart,
		BrokenLamp,

		Map,
		RockFall
	};

	enum class PathCardType
	{
		Undefined,
		WEC, // x4
		SWEC, // x5
		NSWEC, // x5
		SWC, // x4
		NWC, // x5
		WC, // x1
		NWE, // x1
		NSWE, // x1
		SW, // x1
		NW, // x1
		NC, // x1
		NSEC, // x5
		NSC, // x3
		WE, // x1
		NSE, // x1
		NS, // x1
	};

};