#include "PathCard.h"


PathCard::PathCard(const Enums::PathCardType & type, const bool& isGold) :
	type(type), isGold(isGold)
{
	this->pathToCenter = false;
	this->pathToEast = false;
	this->pathToNorth = false;
	this->pathToSouth = false;
	this->pathToWest = false;
	this->isRotate = false;
	setPossiblePaths(type);
}

PathCard::PathCard(PathCard && otherCard)
{
	*this = std::move(otherCard);
}

PathCard::PathCard(const PathCard & otherCard)
{
	*this = otherCard;
}

Enums::PathCardType PathCard::getType()
{
	return this->type;
}

bool PathCard::getPathToNorth()
{
	return this->pathToNorth;
}

bool PathCard::getPathToSouth()
{
	return this->pathToSouth;
}

bool PathCard::getPathToWest()
{
	return this->pathToWest;
}

bool PathCard::getPathToEast()
{
	return this->pathToEast;
}

bool PathCard::getPathToCenter()
{
	return this->pathToCenter;
}

bool PathCard::isFinalGoldCard()
{
	return isGold;
}

bool PathCard::isRotated()
{
	return this->isRotate;
}

void PathCard::setType(Enums::PathCardType type)
{
	this->type = type;
	setPossiblePaths(type);
}

void PathCard::setPathToNorth(const bool & isPath)
{
	this->pathToNorth = isPath;
}

void PathCard::setPathToSouth(const bool & isPath)
{
	this->pathToSouth = isPath;
}

void PathCard::setPathToWest(const bool & isPath)
{
	this->pathToWest = isPath;
}

void PathCard::setPathToEast(const bool & isPath)
{
	this->pathToEast = isPath;
}

void PathCard::setPathToCenter(const bool & isPath)
{
	this->pathToCenter = isPath;
}

void PathCard::setToGold(const bool & isGold)
{
	this->isGold = isGold;
}

void PathCard::setRotate(const bool & rotate)
{
	if (this->isRotate != rotate)
	{
		this->rotate();
		this->isRotate = rotate;
	}
}

PathCard & PathCard::operator=(const PathCard & otherCard)
{
	this->type = otherCard.type;
	this->pathToCenter = otherCard.pathToCenter;
	this->pathToEast = otherCard.pathToEast;
	this->pathToNorth = otherCard.pathToNorth;
	this->pathToWest = otherCard.pathToWest;
	this->pathToSouth = otherCard.pathToSouth;
	this->isGold = otherCard.isGold;
	this->isRotate = otherCard.isRotate;
	return *this;
}

PathCard & PathCard::operator=(PathCard && otherCard)
{
	*(this) = otherCard;
	new(&otherCard) PathCard;
	return *this;
}

void PathCard::setPossiblePaths(const Enums::PathCardType & type)
{
	if ( this->type != type )
		this->type = type;
	
	switch (type)
	{
	case Enums::PathCardType::Undefined:
		setPathToNorth(false);
		setPathToSouth(false);
		setPathToWest(false);
		setPathToEast(false);
		setPathToCenter(false);
		break;
	case Enums::PathCardType::WEC:
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWEC:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSWEC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::SWC:
		setPathToSouth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWC:
		setPathToNorth(true);
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WC:
		setPathToWest(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NWE:
		setPathToNorth(true);
		setPathToWest(true);
		setPathToEast(true);
		break;
	case Enums::PathCardType::NSWE:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToWest(true);
		setPathToEast(true);
		break;
	case Enums::PathCardType::SW:
		setPathToSouth(true);
		setPathToWest(true);
		break;
	case Enums::PathCardType::NW:
		setPathToNorth(true);
		setPathToWest(true);
		break;
	case Enums::PathCardType::NC:
		setPathToNorth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSEC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToEast(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::NSC:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToCenter(true);
		break;
	case Enums::PathCardType::WE:
		setPathToWest(true);
		setPathToEast(true);
		break;
	case Enums::PathCardType::NSE:
		setPathToNorth(true);
		setPathToSouth(true);
		setPathToEast(true);
		break;
	case Enums::PathCardType::NS:
		setPathToNorth(true);
		setPathToSouth(true);
		break;
	default:
		break;
	}
	isRotate = false;
}

void PathCard::rotate()
{
	isRotate = !isRotate;
	if (pathToNorth && !pathToSouth)
	{
		pathToSouth = true;
		pathToNorth = false;
	}
	else if (pathToSouth && !pathToNorth)
	{
		pathToNorth = true;
		pathToSouth = false;
	}

	if (pathToWest && !pathToEast)
	{
		pathToEast = true;
		pathToWest = false;
	}
	else if (pathToEast && !pathToWest)
	{
		pathToWest = true;
		pathToEast = false;
	}
}

void PathCard::Print()
{
}
