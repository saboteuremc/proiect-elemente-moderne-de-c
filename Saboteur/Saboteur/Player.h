#pragma once
#include <iostream>
#include <string>
#include <vector>

#include "Enums.h"
#include "Card.h"
#include "CardDeck.h"
#include "PathCard.h"
	

#include <optional>

class Player
{
	using playerType = bool;
public:
	/* Constructors and Destructor */
	Player(const std::string& name = "Undefined", const int& age = -1, const playerType& type = true);
	Player(const Player& other);

	~Player() = default;

	/* Getters */
	std::string getName();
	int getAge();
	uint8_t getGold();
	playerType getType();
	CardDeck& getCards();
	bool getPickaxeState() const;
	bool getCartState() const;
	bool getLampState() const;
	
	/* Setters */
	void setName(const std::string& name);
	void setAge(const int& age);
	void setType(const playerType& type);
	void setPickaxe(const bool& state);
	void setCart(const bool& state);
	void setLamp(const bool& state);
	void setGold(const uint8_t& gold);

	/* Operators */
	Player& operator =(const Player& other);

private:
	std::string name;
	int age;

	uint8_t gold;
	playerType type;
	CardDeck cards;
	bool intactPickAxe;
	bool intactCart;
	bool intactLamp;
};

