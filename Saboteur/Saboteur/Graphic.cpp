#include "Graphic.h"

Graphic::Graphic()
{
	/* Load the font from file */
	try
	{
		if (!arialFont.loadFromFile("Resources\\arial.ttf"))
		{
			throw "Error: Can't open the Font file!";
		}
	}
	catch (const char* message)
	{
		std::cout << message << std::endl;
	}
	
	/* Load the texture from file */
	try
	{
		if (!primaryCardsTexture.loadFromFile("Resources\\PrimaryCards.jpg"))
		{
			throw "Error: Cannot open the file.";
		}
	}
	catch (const char* message)
	{
		std::cout << message << std::endl;
	}

	/* Load the logo texture from file */
	try
	{
		if (!logoTexture.loadFromFile("Resources\\logo.png"))
		{
			throw "Error: Cannot open the logo texture file.";
		}
	}
	catch (const char* message)
	{
		std::cout << message << std::endl;
	}

	/* Load the background texture from file */
	try
	{
		if (!backgroundTexture.loadFromFile("Resources\\background.jpeg"))
		{
			throw "Error: Cannot open the background texture file.";
		}
	}
	catch (const char* message)
	{
		std::cout << message << std::endl;
	}

	/* Iniatialize the card with the coresponding texture */
	sf::RectangleShape tempCard(sf::Vector2f(cardWidth, cardHeight));
	tempCard.setTexture(&primaryCardsTexture);
	tempCard.rotate(90);
	tempCard.setTextureRect(sf::IntRect(0, 0, cardTextureWidth, cardTextureHeight));
	cardTemplate = tempCard;

	currentPlayerIndex = 0;
	selectedCardIndex = -1;

	/* Draw the Player Type Card */
	playerType = cardTemplate;
	playerType.setRotation(0);
	playerType.setScale(sf::Vector2f(0.5, 0.5));
	playerType.setPosition(sf::Vector2f(832, 450));

	for (int i = 0; i < 6; ++i)
	{
		currentPlayerCards.push_back(sf::RectangleShape(cardTemplate));
	}
}

void Graphic::draw()
{
	displayStartWindow();

	if (exitByForce)
		return;

	displayGameWindow();
}

/* Display start window */
void Graphic::displayStartWindow()
{
	sf::RenderWindow window(sf::VideoMode(width, height), "Saboteur");
	while (window.isOpen())
	{
		window.clear();
		eventHandlerStartWindow(window);

		renderStartWindow(window);

		window.display();
	}
}

void Graphic::eventHandlerStartWindow(sf::RenderWindow & window)
{
	static std::string str;
	static bool enterPressed;
	sf::Event event;
	std::string currentChar;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			exitByForce = true;
			window.close();
		}
		else if (event.type == sf::Event::TextEntered)
		{
			/* Handle 0-9 ASCII characters only and accept just integers between 3-10 */
			sf::Text text;
			currentChar = static_cast<char>(event.text.unicode);
			if (event.text.unicode >= 48 && event.text.unicode <= 57 && numberOfPlayers == 0)
			{
				str += currentChar;
				text.setString(str);

				if (str >= "3" && str <= "9")
				{
					numberOfPlayers = std::stoi(str);
				}
				else if (str == "10")
				{
					numberOfPlayers = std::stoi(str);
				}

				if (str.length() > 1)
					str = "";
			}
			else if (currentPlayerAge.length() < 2 && numberOfPlayers != 0)
			{
				currentChar = static_cast<char>(event.text.unicode);
				if (currentChar == "\r")
					enterPressed = true;

				if (!enterPressed)
					currentPlayerName += currentChar;
				else if ((currentChar >= "0" && currentChar <= "9") || currentChar == "\r")
					if (currentChar != "\r")
						currentPlayerAge += currentChar;
					else if (currentPlayerAge.length() == 1)
						currentPlayerAge = "0" + currentPlayerAge;
			}
			else if (currentPlayerAge.length() >= 2)
			{
				currentPlayerName.clear();
				currentPlayerAge.clear();
				enterPressed = false;
			}
		}
	}
}

void Graphic::renderStartWindow(sf::RenderWindow& window)
{
	sf::String string;
	static int playerCount = 1;

	/* Create the Background */
	sf::RectangleShape background(sf::Vector2f(900, 900));
	background.setTexture(&backgroundTexture);
	background.setTextureRect(sf::IntRect(0, 0, 900, 900));

	/* Create the Logo */
	sf::RectangleShape logo(sf::Vector2f(362, 67));
	logo.setTexture(&logoTexture);
	logo.setTextureRect(sf::IntRect(0, 0, 362, 67));

	if (numberOfPlayers == 0)
	{
		logo.setPosition(sf::Vector2f(130, 170));

		string = "Please insert the number of players...\n                (Between 3-10)";
		sf::Text subTitle(string, arialFont, 20);
		subTitle.setPosition(sf::Vector2f(140, 250));

		window.draw(background);
		window.draw(logo);
		window.draw(subTitle);
	}
	else if (playerCount <= numberOfPlayers)
	{
		logo.setPosition(sf::Vector2f(130, 0));

		string = "Please insert the players name and age...";
		sf::Text subTitle(string, arialFont, 20);
		subTitle.setPosition(sf::Vector2f(120, 80));

		static bool incrementOnce;
		if (currentPlayerAge.length() >= 2 && !incrementOnce)
		{
			playerCount++;
			incrementOnce = true;

			game.getPlayers().push_back(new Player(currentPlayerName, std::stoi(currentPlayerAge)));
		}
		else if (currentPlayerAge == "")
		{
			incrementOnce = false;
		}

		string = "Player ";
		string.insert(string.getSize(), std::to_string(playerCount));
		sf::Text player(string, arialFont, 20);
		player.setPosition(sf::Vector2f(240, 130));

		string = "Name: ";
		string.insert(string.getSize(), currentPlayerName);
		sf::Text name(string, arialFont, 20);
		name.setPosition(sf::Vector2f(200, 170));

		string = "Age: ";
		if (currentPlayerAge.length() > 0 && currentPlayerAge[0] == '0')
		{
			string.insert(string.getSize(), currentPlayerAge[1]);
		}
		else
		{
			string.insert(string.getSize(), currentPlayerAge);
		}
		sf::Text age(string, arialFont, 20);
		age.setPosition(sf::Vector2f(200, 210));

		window.draw(background);
		window.draw(logo);
		window.draw(subTitle);
		window.draw(player);
		window.draw(name);
		window.draw(age);
	}

	if (playerCount > numberOfPlayers && playerCount != 1 && numberOfPlayers != 0)
	{
		window.close();
	}
}


/* Display the game window */
void Graphic::displayGameWindow()
{
	sf::RenderWindow window(initializeGameWindow(), "Saboteur");

	/* Initialize the backend - game */
	game.setNumberOfPlayers(numberOfPlayers);
	game.reset();
	game.initGoldCardDeck();

	/* Set the players name and gold */
	for (Player* player : game.getPlayers())
	{
		allPlayersName.push_back(sf::Text(player->getName(), arialFont));
		allPlayersGold.push_back(sf::Text(std::to_string(player->getGold()), arialFont));
	}

	currentPlayerIndex = game.getMinPlayerIndex();

	/* Game Window Loop */
	while (window.isOpen())
	{
		eventHandlerGameWindow(window);
		window.clear();

		renderBoard(window, primaryCardsTexture);
		renderStatus(window);

		window.display();
	}
}

sf::VideoMode Graphic::initializeGameWindow()
{
	sf::Sprite btn(primaryCardsTexture, sf::IntRect(sf::IntRect(cardTextureWidth * 9, cardTextureHeight * 6, cardTextureWidth, cardTextureHeight)));
	btn.setScale(0.3, 0.3);
	btn.setPosition(sf::Vector2f(nrOfColumns * horizontalGap + 50, 0));

	for (int i = 0; i < Board::kSize; i++)
	{
		boardCards.push_back(sf::RectangleShape(cardTemplate));
	}

	/* Set the gap corresponding to their rotating state */
	if (cardTemplate.getRotation() == 90)
	{
		horizontalGap = cardTemplate.getSize().y + 5;
		verticalGap = cardTemplate.getSize().x + 5;
	}
	else
	{
		horizontalGap = cardTemplate.getSize().x + 5;
		verticalGap = cardTemplate.getSize().y + 5;
	}

	return sf::VideoMode(nrOfColumns * horizontalGap + 400, nrOfRows * verticalGap);
}

void Graphic::setPathTextureIndex(Enums::PathCardType type, float& row, float& col)
{
	switch (type)
	{
	case Enums::PathCardType::NC:
	{
		col = cardTextureWidth * 3;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::NS:
	{
		col = cardTextureWidth * 7;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::NSC:
	{
		row = cardTextureHeight * 3;
		col = 0;
		break;
	}
	case Enums::PathCardType::NSE:
	{
		col = cardTextureWidth * 8;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::NSEC:
	{
		row = cardTextureHeight * 3;
		col = cardTextureWidth * 5;
		break;
	}
	case Enums::PathCardType::NSWE:
	{
		row = cardTextureHeight * 2;
		col = cardTextureWidth * 6;
		break;
	}
	case Enums::PathCardType::NSWEC:
	{
		row = cardTextureHeight * 6;
		col = 0;
		break;
	}
	case Enums::PathCardType::NW:
	{
		row = cardTextureHeight * 2;
		col = cardTextureWidth * 1;
		break;
	}
	case Enums::PathCardType::NWC:
	{
		col = cardTextureWidth * 5;
		row = cardTextureHeight * 4;
		break;
	}
	case Enums::PathCardType::NWE:
	{
		col = cardTextureWidth * 5;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::SW:
	{
		row = cardTextureHeight * 2;
		col = 0;
		break;
	}
	case Enums::PathCardType::SWC:
	{
		row = cardTextureHeight * 4;
		col = 0;
		break;
	}
	case Enums::PathCardType::SWEC:
	{
		col = cardTextureWidth * 5;
		row = cardTextureHeight * 5;
		break;
	}
	case Enums::PathCardType::WC:
	{
		col = cardTextureWidth * 4;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::WE:
	{
		col = cardTextureWidth * 2;
		row = cardTextureHeight * 2;
		break;
	}
	case Enums::PathCardType::WEC:
	{
		row = cardTextureHeight * 5;
		col = 0;
		break;
	}
	default:
	{
		col = cardTextureWidth * 10;
		row = cardTextureHeight * 7;
		break;
	}
	}
}

void Graphic::setActionTextureIndex(ActionCard * actionCard, float & row, float & col)
{
	ActionCard* repairCard = dynamic_cast<ActionCard*>(actionCard);

	if (repairCard != nullptr)
	{
		switch (repairCard->getType())
		{
		case Enums::ActionCardType::RepairPickaxe:
			col = cardTextureWidth * 8;
			row = cardTextureHeight * 0;
			break;
		case Enums::ActionCardType::RepairCart:
			col = cardTextureWidth * 3;
			row = cardTextureHeight * 1;
			break;
		case Enums::ActionCardType::RepairLamp:
			col = cardTextureWidth * 8;
			row = cardTextureHeight * 1;
			break;
		case Enums::ActionCardType::RepairLampAndCart:
			col = cardTextureWidth * 0;
			row = cardTextureHeight * 0;
			break;
		case Enums::ActionCardType::RepairPickaxeAndCart:
			col = cardTextureWidth * 2;
			row = cardTextureHeight * 0;
			break;
		case Enums::ActionCardType::RepairPickaxeAndLamp:
			col = cardTextureWidth * 1;
			row = cardTextureHeight * 0;
			break;
		case Enums::ActionCardType::BrokenPickaxe:
			col = cardTextureWidth * 5;
			row = cardTextureHeight * 0;
			break;
		case Enums::ActionCardType::BrokenCart:
			col = cardTextureWidth * 0;
			row = cardTextureHeight * 1;
			break;
		case Enums::ActionCardType::BrokenLamp:
			col = cardTextureWidth * 5;
			row = cardTextureHeight * 1;
			break;
		case Enums::ActionCardType::Map:
			col = cardTextureWidth * 6;
			row = cardTextureHeight * 6;
			break;
		case Enums::ActionCardType::RockFall:
			col = cardTextureWidth * 3;
			row = cardTextureHeight * 0;
			break;
		default:
			break;
		}
	}
}

void Graphic::renderBoard(sf::RenderWindow & window, sf::Texture& texture)
{
	/* Create the Background */
	sf::RectangleShape background(sf::Vector2f(900, 900));
	background.setTexture(&backgroundTexture);
	background.setTextureRect(sf::IntRect(0, 0, 900, 900));

	window.draw(background);

	int index = 0;
	bool rotateCurrentCard = false;
	for (int i = 0; i < nrOfRows; ++i)
	{
		for (int j = 0; j < nrOfColumns; ++j)
		{
			if (i < nrOfRows && j < nrOfColumns)
			{
				boardCards[index].setTexture(&texture);
				float row = 0, col = 0;
				rotateCurrentCard = false;

				if (game.getBoard()[Board::Position(i, j)] != std::nullopt)
				{
					setPathTextureIndex(game.getBoard()[Board::Position(i, j)]->getType(), row, col);

					if (i == 0 && (j == 0 || j == 2 || j == 4))
					{
						col = cardTextureWidth * 7;
						row = cardTextureHeight * 6;
					}
					else if (i == 8 && j == 2)
					{
						col = cardTextureWidth * 1;
						row = cardTextureHeight * 3;
					}
				}
				else
				{
					col = cardTextureWidth * 11;
					row = cardTextureHeight * 11;
				}

				rotateCurrentCard = game.getBoard()[Board::Position(i, j)]->isRotated();

				if (rotateCurrentCard)
					boardCards[index].setRotation(270);
				else
					boardCards[index].setRotation(90);

				boardCards[index].setTextureRect(sf::IntRect(col, row, cardTextureWidth, cardTextureHeight));
			}
			else
			{
				boardCards[index].setTexture(nullptr);
			}
			if (rotateCurrentCard)
				boardCards[index].setPosition((j * horizontalGap) + 5, (i * verticalGap) + verticalGap - 5);
			else
				boardCards[index].setPosition((j * horizontalGap) + horizontalGap, i * verticalGap);

			window.draw(boardCards[index]);
			index++;
		}
	}
}

void Graphic::renderStatus(sf::RenderWindow & window)
{
	/* Create the First Button */
	sf::String string;
	string = "Rotate";
	rotateButton.setString(string);
	rotateButton.setFont(arialFont);
	rotateButton.setPosition(sf::Vector2f(550, 20));

	/* Create the Second Button */
	string = "Pass";
	passButton.setString(string);
	passButton.setFont(arialFont);
	passButton.setPosition(sf::Vector2f(700, 20));

	/* Render the current Player's hand cards */
	CardDeck& currentCardDeck = game.getPlayers()[currentPlayerIndex]->getCards();
	float row = 0, col = 0;
	bool rotation;
	for (int i = 0; i < currentCardDeck.getSize(); ++i)
	{
		currentPlayerCards[i].setPosition((horizontalGap * 5) + (verticalGap * i) + 5, 500);

		PathCard* pathCard = dynamic_cast<PathCard*>(currentCardDeck[i].value());
		ActionCard* actionCard = dynamic_cast<ActionCard*>(currentCardDeck[i].value());
		if (pathCard != nullptr)
		{
			setPathTextureIndex(pathCard->getType(), row, col);
		}
		else if (actionCard != nullptr)
		{
			setActionTextureIndex(actionCard, row, col);
		}

		currentPlayerCards[i].setTextureRect(sf::IntRect(col, row, cardTextureWidth, cardTextureHeight));
		currentPlayerCards[i].setRotation(0);
		window.draw(currentPlayerCards[i]);
	}

	/* Render all the Players and their status */
	for (int i = 0; i < numberOfPlayers; i++)
	{
		allPlayersName[i].setPosition((horizontalGap * 5) + 5, 100 + (i * 35));
		allPlayersName[i].setCharacterSize(25);
		window.draw(allPlayersName[i]);

		int indexOfStateCards = 1;

		/* Draw the Gold Nugget Card */
		sf::RectangleShape goldCard(cardTemplate);
		goldCard.setRotation(0);
		goldCard.setScale(sf::Vector2f(0.5, 0.5));
		goldCard.setTextureRect(sf::IntRect(cardTextureWidth * 8, cardTextureHeight * 6, cardTextureWidth, cardTextureHeight));
		goldCard.setPosition((horizontalGap * 5) + 100 + (35 * indexOfStateCards), 100 + (i * 35));
		window.draw(goldCard);

		allPlayersGold[i].setString(std::to_string(game.getPlayers()[i]->getGold()));
		allPlayersGold[i].setPosition((horizontalGap * 5) + 100 + (35 * indexOfStateCards), 100 + (i * 35));
		allPlayersGold[i].setCharacterSize(25);
		window.draw(allPlayersGold[i]);
		indexOfStateCards++;

		/* Draw each Action Card of the player */
		float row = 0, col = 0;
		sf::RectangleShape stateCard(cardTemplate);
		stateCard.setRotation(0);
		stateCard.setScale(sf::Vector2f(0.5, 0.5));
		if (!game.getPlayers()[i]->getPickaxeState())
		{
			row = cardTextureWidth * 5;
			stateCard.setTextureRect(sf::IntRect(row, col, cardTextureWidth, cardTextureHeight));
			col = cardTextureHeight * 0;
			stateCard.setPosition((horizontalGap * 5) + 100 + (35 * indexOfStateCards), 100 + (i * 35));
			indexOfStateCards++;
			window.draw(stateCard);
		}
		if (!game.getPlayers()[i]->getCartState())
		{
			row = cardTextureWidth * 0;
			col = cardTextureHeight * 1;
			stateCard.setTextureRect(sf::IntRect(row, col, cardTextureWidth, cardTextureHeight));
			stateCard.setPosition((horizontalGap * 5) + 100 + (35 * indexOfStateCards), 100 + (i * 35));
			indexOfStateCards++;
			window.draw(stateCard);
		}
		if (!game.getPlayers()[i]->getLampState())
		{
			row = cardTextureWidth * 5;
			col = cardTextureHeight * 1;
			stateCard.setTextureRect(sf::IntRect(row, col, cardTextureWidth, cardTextureHeight));
			stateCard.setPosition((horizontalGap * 5) + 100 + (35 * indexOfStateCards), 100 + (i * 35));
			indexOfStateCards++;
			window.draw(stateCard);
		}
	}

	/* MapCard display count down */
	if (revealCardCountDown != -1)
	{
		revealCardCountDown--;
		window.draw(revealCard);
	}

	/* Draw the current Player name */
	sf::Text displayedPlayerName;
	displayedPlayerName.setString(game.getPlayers()[currentPlayerIndex]->getName());
	displayedPlayerName.setFont(arialFont);
	displayedPlayerName.setScale(0.8, 0.8);
	displayedPlayerName.setPosition(sf::Vector2f(625, 460));

	window.draw(playerType);
	window.draw(displayedPlayerName);
	window.draw(rotateButton);
	window.draw(passButton);

	if (game.getRound() == 3)
	{
		sf::Text gameOverCaption;
		gameOverCaption.setString("GAME OVER");
		gameOverCaption.setFont(arialFont);
		gameOverCaption.setPosition(sf::Vector2f(250, 250));
		gameOverCaption.setScale(2.5, 2.5);
		gameOverCaption.setFillColor(sf::Color::Red);

		window.draw(gameOverCaption);
	}
}

void Graphic::eventHandlerGameWindow(sf::RenderWindow& window)
{
	if (game.getRound() == 3)
	{
		return;
	}

	sf::Event event;
	sf::Color hoverColor = sf::Color::Red;
	sf::Color whiteColor = sf::Color::White;

	static Board::Position position;

	while (window.pollEvent(event))
	{
		//==================================< HOVER EVENT HANDLER >=====================================//

		/* Hover color for the Board Cards */
		for (sf::RectangleShape card : boardCards)
		{
			if (isMouseWithinShape(card, window))
			{
				card.setFillColor(hoverColor);
			}
			else
			{
				card.setFillColor(whiteColor);
			}
		}

		/* Hover color for All Players Name */
		for (int i = 0; i < numberOfPlayers; i++)
		{
			if (isMouseWithinShape(allPlayersName[i], window))
				allPlayersName[i].setFillColor(hoverColor);
			else
				allPlayersName[i].setFillColor(whiteColor);
		}

		/* Hover color for the First Button */
		if (isMouseWithinShape(rotateButton, window))
			rotateButton.setFillColor(hoverColor);
		else
			rotateButton.setFillColor(whiteColor);

		/* Hover color for the Second Button */
		if (isMouseWithinShape(passButton, window))
			passButton.setFillColor(hoverColor);
		else
			passButton.setFillColor(whiteColor);


		if (isMouseWithinShape(playerType, window))
		{
			if (game.getPlayers()[currentPlayerIndex]->getType() == game.GOLD_MINER)
			{
				playerType.setTextureRect(sf::IntRect(cardTextureWidth * 7, cardTextureHeight * 7, cardTextureWidth, cardTextureHeight));
			}
			else
			{
				playerType.setTextureRect(sf::IntRect(cardTextureWidth * 8, cardTextureHeight * 7, cardTextureWidth, cardTextureHeight));
			}
		}
		else
		{
			playerType.setTextureRect(sf::IntRect(cardTextureWidth * 9, cardTextureHeight * 7, cardTextureWidth, cardTextureHeight));
		}

		//=================================< BUTTON PRESS EVENT HANDLER >====================================//

		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (event.type == sf::Event::MouseButtonPressed)
		{
			/* Check if the Cursor have clicked one of the cards from the Board */
			int cardIndex = 0;
			for (sf::RectangleShape card : boardCards)
			{
				if (isMouseWithinShape(card, window) && selectedCardIndex != -1)
				{
					Card* currentPlayerCard = game.getPlayers()[currentPlayerIndex]->getCards()[selectedCardIndex].value();

					PathCard* pathCard = dynamic_cast<PathCard*>(currentPlayerCard);
					ActionCard* actionCard = dynamic_cast<ActionCard*>(currentPlayerCard);

					position = Board::Position(cardIndex / Board::kWidth, cardIndex % Board::kWidth);

					/* Check if the selected Card from hand was a PathCard and put it on the Board */
					if (pathCard != nullptr && game.canPutCardOnBoard(game.getBoard(), position, pathCard) &&
						game.getPlayers()[currentPlayerIndex]->getCartState() &&
						game.getPlayers()[currentPlayerIndex]->getLampState() &&
						game.getPlayers()[currentPlayerIndex]->getPickaxeState())
					{
						game.getBoard().getCards()[cardIndex] = *pathCard;
						extractCardFromHand = true;
					}

					/* Check if the selected Card from hand was an MapCard */
					if (actionCard != nullptr && actionCard->getType() == Enums::ActionCardType::Map)
					{
						actionCard->executeAction(game.getPlayers()[currentPlayerIndex], game.getBoard(), position);
						if (position != Board::Position(-1, -1))
						{
							float col = 0, row = 0;
							if (game.getBoard()[position].value().isFinalGoldCard())
							{
								col = cardTextureWidth * 2;
								row = cardTextureHeight * 3;
							}
							else
							{
								col = cardTextureWidth * 3;
								row = cardTextureHeight * 3;
							}
							revealCard = cardTemplate;
							revealCard.setPosition(350, 150);
							revealCard.setRotation(0);
							revealCard.setScale(3, 3);
							revealCard.setTextureRect(sf::IntRect(col, row, cardTextureWidth, cardTextureHeight));
							revealCardCountDown = 1000;

							extractCardFromHand = true;
						}
					}

					/* Check if the selected Card from hand was a RockFallCard */
					if (actionCard != nullptr && actionCard->getType() == Enums::ActionCardType::RockFall)
					{
						if (position != Board::Position(0, 0) &&
							position != Board::Position(0, 2) &&
							position != Board::Position(0, 4) &&
							position != Board::Position(8, 2) &&
							game.getBoard()[position].has_value())
						{
							actionCard->executeAction(game.getPlayers()[currentPlayerIndex], game.getBoard(), position);
							if (position != Board::Position(-1, -1))
								extractCardFromHand = true;
						}
					}
				}
				cardIndex++;
			}

			/* Check if the Cursor have clicked one of the cards from the Player Hands */
			CardDeck& currentCardDeck = game.getPlayers()[currentPlayerIndex]->getCards();
			for (int i = 0; i < currentCardDeck.getSize(); ++i)
			{
				if (isMouseWithinShape(currentPlayerCards[i], window))
				{
					/* Handle the switch between the Active/Inactive state of the Player cards hands */
					if (currentPlayerCards[i].getFillColor() == hoverColor)
					{
						currentPlayerCards[i].setFillColor(whiteColor);
						selectedCardIndex = -1;
					}
					else if (selectedCardIndex == -1)
					{
						currentPlayerCards[i].setFillColor(hoverColor);
						selectedCardIndex = i;
					}
				}
			}

			/* Check if the Cursor have clicked on one of the Player Name */
			for (int i = 0; i < numberOfPlayers; i++)
			{
				if (isMouseWithinShape(allPlayersName[i], window) && selectedCardIndex != -1)
				{
					ActionCard* actionCard = dynamic_cast<ActionCard*>(game.getPlayers()[currentPlayerIndex]->getCards()[selectedCardIndex].value());
					if (actionCard != nullptr)
					{
						if (actionCard->getType() == Enums::ActionCardType::BrokenCart ||
							actionCard->getType() == Enums::ActionCardType::BrokenLamp ||
							actionCard->getType() == Enums::ActionCardType::BrokenPickaxe)
						{
							actionCard->executeAction(game.getPlayers()[i], game.getBoard(), position);
							if (position != Board::Position(-1, -1))
								extractCardFromHand = true;
						}
						else if (actionCard->getType() == Enums::ActionCardType::RepairCart ||
							actionCard->getType() == Enums::ActionCardType::RepairLamp ||
							actionCard->getType() == Enums::ActionCardType::RepairLampAndCart ||
							actionCard->getType() == Enums::ActionCardType::RepairPickaxe ||
							actionCard->getType() == Enums::ActionCardType::RepairPickaxeAndCart ||
							actionCard->getType() == Enums::ActionCardType::RepairPickaxeAndLamp)
						{
							actionCard->executeAction(game.getPlayers()[i], game.getBoard(), position);
							if (position != Board::Position(-1, -1))
								extractCardFromHand = true;
						}
					}
				}
			}

			/* Check if the Cursor have clicked the First Button */
			if (isMouseWithinShape(rotateButton, window) && selectedCardIndex != -1)
			{
				Card* currentPlayerCard = game.getPlayers()[currentPlayerIndex]->getCards()[selectedCardIndex].value();

				PathCard* pathCard = dynamic_cast<PathCard*>(currentPlayerCard);
				if (pathCard != nullptr)
				{
					pathCard->rotate();
				}
			}

			/* Check if the Cursor have clicked the Second Button */
			if (isMouseWithinShape(passButton, window))
			{
				if (selectedCardIndex != -1)
					extractCardFromHand = true;
			}

			if (extractCardFromHand)
			{
				game.discardCard(currentPlayerIndex, selectedCardIndex);
				game.verifyIfRoundIsOver(currentPlayerIndex);
				currentPlayerIndex = (currentPlayerIndex + 1) % numberOfPlayers;
				selectedCardIndex = -1;
				extractCardFromHand = false;

				CardDeck& currentCardDeck = game.getPlayers()[currentPlayerIndex]->getCards();
				for (int i = 0; i < currentCardDeck.getSize(); ++i)
				{
					/* Handle the switch between the Active/Inactive state of the Player cards hands */
					currentPlayerCards[i].setFillColor(whiteColor);
				}
			}
		}


		//=====================================================================//
	}
}
