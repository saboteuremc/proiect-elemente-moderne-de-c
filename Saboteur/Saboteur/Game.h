#pragma once

#include <vector>
#include <memory>
#include <algorithm>

#include <iterator>
#include <random>


//#include "../../DLL/DLL/SaboteurV1.h"
//#include <fstream> 

#include "Board.h"
#include "Player.h"
#include "CardDeck.h"

#include "PathCard.h"
#include "ActionCard.h"

class Game
{
//private:
public:
	const bool GOLD_MINER = true;
	const bool SABOTEUR = false;
public:
	/* Constructor */
	Game();

	/* Getters */
	Board& getBoard();
	std::vector<Player*>& getPlayers();
	CardDeck& getPathCardDeck();
	CardDeck& getDiscardDeck();
	std::vector<bool>& getDwarfCardDeck();
	std::vector<uint8_t>& getGoldCardDeck();
	int getNumberOfPlayers();
	int getRound();

	/* Setters */
	void setBoard(Board board);
	void setPlayers(std::vector<Player*> players);
	void setPathCardDeck(CardDeck pathCardDeck);
	void setDiscardDeck(CardDeck discardDeck);
	void setDwarfCardDeck(std::vector<bool> dwarfCardDeck);
	void setGoldcardDeck(std::vector<uint8_t> goldCardDeck);
	void setNumberOfPlayers(const int& numberOfPlayers);

public:
	void initPathCardDeck();
	void initPathCards(int& index);
	void initActionCards(int& index);

	void initDwarfCardDeck();

	void distributePathCard(const int& numberOfPlayers, Player* player, int& index);
	void distributeDwarfCards();
	void distributePathCards();


	void distributeGold(std::vector<Player*>::iterator& it);
	bool allPlayersHaveEmptyHands();


public:
	void discardCard(const int& index, const int& choiceIndex);
	void verifyIfRoundIsOver(const int& currentPlayeIndex);
	bool canPutCardOnBoard(Board board, Board::Position position, PathCard* pathCard);
	void initGoldCardDeck();
	int getMinPlayerIndex();
	void reset();

private:
	Board board;
	std::vector<Player*> players;

	CardDeck pathCardDeck;
	CardDeck discardDeck;

	std::vector<bool> dwarfCardDeck;
	std::vector<uint8_t> goldCardDeck;
	int goldCardDeckIndex = goldCardDeck.size() - 1;
	int numberOfPlayers;

	int round;
};

