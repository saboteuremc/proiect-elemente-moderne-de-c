#include "CardDeck.h"
#include <algorithm>

#include <iterator>
#include <random>

CardDeck::CardDeck(const int& size)
{
	setSize(size);
}

CardDeck::CardDeck(const CardDeck & other)
{
	*(this) = other;
}

CardDeck::CardDeck(CardDeck && other)
{
	*(this) = std::move(other);
}

CardDeck & CardDeck::operator=(const CardDeck & other)
{
	this->deck = other.deck;
	return *(this);
}

CardDeck & CardDeck::operator=(CardDeck && other)
{
	this->deck = other.deck;

	new(&other) CardDeck;

	return *(this);
}

void CardDeck::shuffle()
{
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(deck.begin(), deck.end(), generator);
}

void CardDeck::setSize(const int & size)
{
	this->deck.reserve(size);
	this->deck.resize(size);
}

const std::optional<Card*>& CardDeck::operator[](const int & index) const
{
	if (index < 0 || index >= deck.size())
		throw "Deck index out of bound.";

	return deck[index];
}

std::vector<std::optional<Card*>>& CardDeck::getDeck()
{
	return deck;
}

int CardDeck::getSize()
{
	return this->deck.size();
}

std::optional<Card*>& CardDeck::operator[](const int & index)
{
	if (index < 0 || index >= deck.size())
		throw "Deck index out of bound.";

	return deck[index];
}
