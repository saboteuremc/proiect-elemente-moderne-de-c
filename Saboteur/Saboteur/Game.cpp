#include "Game.h"
//for the dll
#include "../../DLL/DLL/SaboteurV1.h"
#pragma comment(lib, "DLL.lib")
#include <fstream> 
std::ofstream of("syslog2.log", std::ios::app);
std::string myString = "";
Saboteur mySaboteurDLL(of);
auto dllCall = [](std::string& myString, Saboteur mySaboteurDLL,Saboteur::Actions actionType, Saboteur::Type type)
{
	myString = mySaboteurDLL.showMessage(actionType, type);
	mySaboteurDLL.log(myString);
};
void Game::initPathCardDeck()
{
	int index = 0;
	initPathCards(index);
	initActionCards(index);

}

void Game::initPathCards(int& index)
{
	this->pathCardDeck.setSize(67);

	auto addCard = [](const int& count, const Enums::PathCardType& type, CardDeck& pathCardDeck, int& index)
	{
		for (int i = 0; i < count; i++)
		{
			pathCardDeck[index++] = new PathCard(type); 
		};
	};

	addCard(5, Enums::PathCardType::SWEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NSWEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NSEC, pathCardDeck, index);
	addCard(5, Enums::PathCardType::NWC, pathCardDeck, index);


	addCard(4, Enums::PathCardType::WEC, pathCardDeck, index);
	addCard(4, Enums::PathCardType::SWC, pathCardDeck, index);

	addCard(3, Enums::PathCardType::NSC, pathCardDeck, index);
	
	addCard(1, Enums::PathCardType::WC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NW, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NWE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::SW, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NC, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NS, pathCardDeck, index);
	addCard(1, Enums::PathCardType::NSWE, pathCardDeck, index);
	addCard(1, Enums::PathCardType::WE, pathCardDeck, index);

}

void Game::initActionCards(int& index)
{

	auto addCard = [](const int& count, const Enums::ActionCardType& type, CardDeck& pathCardDeck, int& index)
	{
		for (int i = 0; i < count; i++)
		{
			pathCardDeck[index++] = new ActionCard(type);
		};
	};

	addCard(2, Enums::ActionCardType::RepairPickaxe, pathCardDeck, index);
	addCard(2, Enums::ActionCardType::RepairCart, pathCardDeck, index);
	addCard(2, Enums::ActionCardType::RepairLamp, pathCardDeck, index);

	addCard(3, Enums::ActionCardType::BrokenPickaxe, pathCardDeck, index);
	addCard(3, Enums::ActionCardType::BrokenCart, pathCardDeck, index);
	addCard(3, Enums::ActionCardType::BrokenLamp, pathCardDeck, index);

	addCard(6, Enums::ActionCardType::Map, pathCardDeck, index);

	addCard(3, Enums::ActionCardType::RockFall, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairPickaxeAndCart, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairLampAndCart, pathCardDeck, index);

	addCard(1, Enums::ActionCardType::RepairPickaxeAndLamp, pathCardDeck, index);
}

void Game::initGoldCardDeck()
{

	auto addCard = [](const int& count, const int& amount, std::vector<uint8_t>& goldCardDeck)
	{
		for (int i = 0; i < count; i++)
		{
			goldCardDeck.push_back(amount);
		};
	};

	int goldCardNumbers = 17;
	addCard(goldCardNumbers, 1, goldCardDeck);

	goldCardNumbers = 7;
	addCard(goldCardNumbers, 2, goldCardDeck);

	goldCardNumbers = 4;
	addCard(goldCardNumbers, 3, goldCardDeck);

}

void Game::initDwarfCardDeck()
{
	if (dwarfCardDeck.size() != 0)
	{
		dwarfCardDeck.clear();
	}

	auto addCard = [](const int& count, const bool& type, std::vector<bool>& dwarfCardDeck)
	{
		for (int i = 0; i < count; i++)
		{
			dwarfCardDeck.push_back(type);
		};
	};

	switch (numberOfPlayers)
	{
	case 3:
		addCard(1, SABOTEUR, dwarfCardDeck);
		addCard(3, GOLD_MINER, dwarfCardDeck);
		break;

	case 4:
		addCard(1, SABOTEUR, dwarfCardDeck);
		addCard(4, GOLD_MINER, dwarfCardDeck);
		break;

	case 5:
		addCard(2, SABOTEUR, dwarfCardDeck);
		addCard(4, GOLD_MINER, dwarfCardDeck);
		break;

	case 6:
		addCard(2, SABOTEUR, dwarfCardDeck);
		addCard(5, GOLD_MINER, dwarfCardDeck);
		break;

	case 7:
		addCard(3, SABOTEUR, dwarfCardDeck);
		addCard(5, GOLD_MINER, dwarfCardDeck);
		break;

	case 8:
		addCard(3, SABOTEUR, dwarfCardDeck);
		addCard(6, GOLD_MINER, dwarfCardDeck);
		break;

	case 9:
		addCard(3, SABOTEUR, dwarfCardDeck);
		addCard(7, GOLD_MINER, dwarfCardDeck);
		break;

	case 10:
		addCard(4, SABOTEUR, dwarfCardDeck);
		addCard(7, GOLD_MINER, dwarfCardDeck);
		break;

	default:
		break;
	}
	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(dwarfCardDeck.begin(), dwarfCardDeck.end(), generator);
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::cardsShuffled, Saboteur::Type::Info);
}

void Game::distributePathCard(const int & numberOfPlayers, Player * player, int& index)
{
	int numberOfCards;

	if (numberOfPlayers >= 3 && numberOfPlayers <= 5)
	{
		numberOfCards = 6;
	}
	else if (numberOfPlayers >= 6 && numberOfPlayers <= 7)
	{
		numberOfCards = 5;
	}
	else if (numberOfPlayers >= 8 && numberOfPlayers <= 10)
	{
		numberOfCards = 4;
	}

	player->getCards().setSize(numberOfCards);
	for (int i = 0; i < numberOfCards; i++)
	{
		player->getCards()[i] = std::move(pathCardDeck[index++]);
		pathCardDeck.getDeck().erase(pathCardDeck.getDeck().begin() + i);
	}
	index = 0;
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::playersReady, Saboteur::Type::Info);
}

Game::Game()
{
	this->round = 0;
	this->pathCardDeck.setSize(67);
}



bool Game::allPlayersHaveEmptyHands()
{
	return std::all_of(players.begin(), players.end(), [](Player* player) {return player->getCards().getDeck().empty(); });
}


Board& Game::getBoard()
{
	return this->board;
}

std::vector<Player*>& Game::getPlayers()
{
	return this->players;
}

CardDeck& Game::getPathCardDeck()
{
	return this->pathCardDeck;
}

CardDeck& Game::getDiscardDeck()
{
	return this->discardDeck;
}

std::vector<bool>& Game::getDwarfCardDeck()
{
	return this->dwarfCardDeck;
}

std::vector<uint8_t>& Game::getGoldCardDeck()
{
	return this->goldCardDeck;
}

int Game::getNumberOfPlayers()
{
	return this->numberOfPlayers;
}

int Game::getRound()
{
	return this->round;
}

void Game::setBoard(Board board)
{
	this->board = board;
}

void Game::setPlayers(std::vector<Player*> players)
{
	this->players = players;
}

void Game::setPathCardDeck(CardDeck pathCardDeck)
{
	this->pathCardDeck = pathCardDeck;
}

void Game::setDiscardDeck(CardDeck discardDeck)
{
	this->discardDeck = discardDeck;
}

void Game::setDwarfCardDeck(std::vector<bool> dwarfCardDeck)
{
	this->dwarfCardDeck = dwarfCardDeck;
}

void Game::setGoldcardDeck(std::vector<uint8_t> goldCardDeck)
{
	this->goldCardDeck = goldCardDeck;
}

void Game::setNumberOfPlayers(const int & numberOfPlayers)
{
	this->numberOfPlayers = numberOfPlayers;
}

bool Game::canPutCardOnBoard(Board board, Board::Position position, PathCard* pathCard)
{
	auto northPosition = Board::Position(position.first - 1, position.second);
	auto southPosition = Board::Position(position.first + 1, position.second);
	auto westPosition = Board::Position(position.first, position.second - 1);
	auto eastPosition = Board::Position(position.first, position.second + 1);

	bool north = false, south = false, east = false, west = false;

	if (board[position] == std::nullopt)
	{

		//verify all if null
		try
		{
			if (board.inBound(northPosition) && board[northPosition] == std::nullopt &&
				board.inBound(southPosition) && board[southPosition] == std::nullopt &&
				board.inBound(eastPosition) && board[eastPosition] == std::nullopt &&
				board.inBound(westPosition) && board[westPosition] == std::nullopt)
			{
				return false;
			}
		}
		catch (const char* message)
		{
			return false;
		}

		//north
		if (!board.inBound(northPosition))
		{
			north = true;
		}
		else
		{
			if (board.inBound(northPosition) && board[northPosition] == std::nullopt)

			{
				north = true;
			}
			else
				if ((board[northPosition].value().getPathToSouth() && pathCard->getPathToNorth()) || 
					(!board[northPosition].value().getPathToSouth() && !pathCard->getPathToNorth()) )
				{
					north = true;
				}
		}


		//verify west
		if (!board.inBound(westPosition))
		{
			west = true;
		}
		else
		{

			if (board.inBound(westPosition) && board[westPosition] == std::nullopt)
			{
				west = true;
			}
			else
				if ((board[westPosition].value().getPathToEast() && pathCard->getPathToWest()) ||
					(!board[westPosition].value().getPathToEast() && !pathCard->getPathToWest()))
				{
					west = true;
				}
		}


		//verify south
		if (!board.inBound(southPosition))
		{
			south = true;
		}
		else
		{

			if (board.inBound(southPosition) && board[southPosition] == std::nullopt)
			{
				south = true;
			}
			else if ((board[southPosition].value().getPathToNorth() && pathCard->getPathToSouth()) ||
					(!board[southPosition].value().getPathToNorth() && !pathCard->getPathToSouth()))
			{
				south = true;
			}
		}


		//verify east
		if (!board.inBound(eastPosition))
		{
			east = true;
		}
		else
		{

			if (board.inBound(eastPosition) && board[eastPosition] == std::nullopt)
			{
				east = true;
			}
			else if ((board[eastPosition].value().getPathToWest() && pathCard->getPathToEast()) ||
					(!board[eastPosition].value().getPathToWest() && !pathCard->getPathToEast()))
			{
				east = true;
			}
		}

	}

	return (east && north && west && south);
}

void Game::distributeDwarfCards()
{
	int count = 0;
	for (auto player : players)
	{
		player->setType(dwarfCardDeck[count]);
		count++;
	}
}

void Game::distributePathCards()
{
	pathCardDeck.shuffle();
	int index = 0;
	for (auto player : players)
	{
		distributePathCard(numberOfPlayers, player, index);
	}
}

void Game::distributeGold(std::vector<Player*>::iterator& it)
{
	
	std::vector<uint8_t> goldStones;
	for (int i = 0; i < players.size(); i++)
	{
		goldStones.push_back(goldCardDeck[goldCardDeck.size() - 1 - i]);
		goldCardDeck.pop_back();
	}

	if ((*it)->getType() == GOLD_MINER)
	{
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::diggersWon, Saboteur::Type::Info);
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::goldDistribution, Saboteur::Type::Info);
		int count = 0;
		while (!goldStones.empty() && count != numberOfPlayers)
		{
			if ((*it)->getType())
			{
				auto maxGoldNugget = std::max_element(std::begin(goldStones), std::end(goldStones));
				(*it)->setGold((*it)->getGold() + *maxGoldNugget);
				goldStones.erase(maxGoldNugget);
			}
			it++;
			if (it == players.end())
			{
				it = players.begin();
			}
			count++;
		}
	}
	else
		if ((*it)->getType() == SABOTEUR)
		{
			dllCall(myString, mySaboteurDLL, Saboteur::Actions::saboteursWon, Saboteur::Type::Info);
			dllCall(myString, mySaboteurDLL, Saboteur::Actions::goldDistribution, Saboteur::Type::Info);
			int sum = 0;
			for (auto player : players)
			{
				if (!player->getType())
				{
					player->setGold(player->getGold() + 3);
					sum += 3;
				}
			}

			auto rIndex = goldCardDeck.size() - 1;
			while (sum > 0)
			{
				if (sum - goldCardDeck[rIndex] < 0)
				{
					rIndex--;
				}
				else
				{
					sum -= goldCardDeck[rIndex];
					goldCardDeck.erase(goldCardDeck.begin() + rIndex);
					rIndex--;
				}

			}
		}
}

void Game::verifyIfRoundIsOver(const int& currentPlayerIndex)
{
	Player* currentPlayer = players[currentPlayerIndex];
	if (board.pathReachedGold(board) == true)
	{
		dllCall(myString, mySaboteurDLL, Saboteur::Actions::roundEnded, Saboteur::Type::Info);
		auto it = std::find(players.begin(), players.end(), currentPlayer);

		distributeGold(it);
		this->reset();

		this->round++;
	}
	else if (allPlayersHaveEmptyHands())
	{
		int position = -1;
		for (int i = 0; i < players.size(); i++)
		{
			if (players[i]->getType() == SABOTEUR)
				position = i;
		}
		auto it = players.begin() + position;

		if (position != -1)
		{
			distributeGold(it);
		}
		this->reset();

		this->round++;
	}
}


void Game::reset()
{
	this->board.InitializeBoard();
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::boardCreated, Saboteur::Type::Info);
	this->initPathCardDeck();
	this->initDwarfCardDeck();
	this->distributeDwarfCards();
	this->distributePathCards();
}

void Game::discardCard(const int& index, const int& choiceIndex)
{
	auto& playerCards = players[index]->getCards().getDeck();
	auto begin = playerCards.begin();
	playerCards.erase(begin + choiceIndex);
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::currentPlayer, Saboteur::Type::Info);
	myString = " "+players[index]->getName();
	mySaboteurDLL.log(myString);
	

	if (!pathCardDeck.getDeck().empty())
	{
		playerCards.push_back(std::move(pathCardDeck[pathCardDeck.getDeck().size() - 1]));
		pathCardDeck.getDeck().erase(pathCardDeck.getDeck().end() - 1);
	}
	dllCall(myString, mySaboteurDLL, Saboteur::Actions::playerChoose, Saboteur::Type::Info);
}

int Game::getMinPlayerIndex()
{
	auto min = std::min_element(players.begin(), players.end(),
		[](Player* player1, Player* player2)
	{
		return player1->getAge() < player2->getAge();
	});

	return std::distance(players.begin(), min);
}