#pragma once
#include<array>
#include<optional>
#include "PathCard.h"
class Board
{
public:
	static const size_t kWidth = 5;
	static const size_t kHeight = 9;
	static const size_t kSize = kWidth * kHeight;

public:
	using Position = std::pair<uint8_t, uint8_t>;
public:
	Board() ;
	
	//Getter
	const std::optional<PathCard>& operator[] (const Position& pos) const;

	//Getter or/and Setter
	std::optional<PathCard>& operator[] (const Position& pos);
	std::array<std::optional<PathCard>, kSize>& getCards();

	void InitializeBoard();

public:
	 bool pathReachedGold(Board& board);
	 bool inBound(Board::Position position);

private:
	std::array<std::optional<PathCard>, kSize> m_cards;
};

