#pragma once

#include <vector>
#include <optional>
#include "Card.h"
class CardDeck
{
public:
	CardDeck() = default;
	CardDeck(const int& size);
	~CardDeck() = default;
	CardDeck(const CardDeck& other);
	CardDeck(CardDeck&& other);
	CardDeck& operator = (const CardDeck& other);
	CardDeck& operator = ( CardDeck&& other);

	//Getter
	const std::optional<Card*>& operator[] (const int& index) const;
	std::vector<std::optional<Card*>>& getDeck();
	int getSize();

	//Getter or/and Setter
	std::optional<Card*>& operator[] (const int& index);

	// Useful Functions
	void shuffle();
	void setSize(const int& size);

private:
	std::vector < std::optional<Card*> > deck;
};
