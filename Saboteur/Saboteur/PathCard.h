#pragma once
#include "Card.h"

class PathCard : public Card
{
public:
	/* Constructors */
	PathCard(const Enums::PathCardType& type = Enums::PathCardType::Undefined, const bool& isGold=false);
	PathCard(PathCard&& otherCard);
	PathCard(const PathCard& otherCard);

	/* Getter */
	Enums::PathCardType getType();
	bool getPathToNorth();
	bool getPathToSouth();
	bool getPathToWest();
	bool getPathToEast();
	bool getPathToCenter();
	bool isFinalGoldCard();
	bool isRotated();

	/* Setter */
	void setType(Enums::PathCardType type);
	void setPathToNorth(const bool& isPath);
	void setPathToSouth(const bool& isPath);
	void setPathToWest(const bool& isPath);
	void setPathToEast(const bool& isPath);
	void setPathToCenter(const bool& isPath);
	void setToGold(const bool& isGold);
	void setRotate(const bool& rotate);

	/* Operators */
	PathCard& operator=(const PathCard& otherCard);
	PathCard& operator=(PathCard&& otherCard);

	/* Useful Functions */
	void setPossiblePaths(const Enums::PathCardType& type);
	void rotate();
	void Print() override;

	/* Destructor */
	~PathCard() = default;
private:
	Enums::PathCardType type;
	bool pathToNorth;
	bool pathToSouth;
	bool pathToWest;
	bool pathToEast;
	bool pathToCenter;
	bool isGold;
	bool isRotate;
};

