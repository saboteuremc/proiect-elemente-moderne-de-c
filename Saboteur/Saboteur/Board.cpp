#include "Board.h"
#include <vector>
#include <algorithm>
#include <queue>
#include <iterator>
#include <random>
#include "Graphic.h"



Board::Board()
{
	InitializeBoard();
}

const std::optional<PathCard>& Board::operator[](const Position & position) const
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
		throw "Board index out of bound.";

	return m_cards[line * kWidth + column];
}


std::optional<PathCard>& Board::operator[](const Position & position)
{
	const auto&[line, column] = position;

	if (line >= kHeight || column >= kWidth)
	{
		throw "Board index out of bound.";
	}

	return m_cards[line * kWidth + column];
}

std::array<std::optional<PathCard>, Board::kSize>& Board::getCards()
{
	return this->m_cards;
}

/* Function to shuffle every round the final cards*/
void Board::InitializeBoard()
{
	for (auto& position : this->m_cards)
	{
		position = std::nullopt;
	}

	std::vector<Board::Position> finalCards;
	finalCards.resize(3);
	finalCards[0] = Board::Position(0, 0);
	finalCards[1] = Board::Position(0, 2);
	finalCards[2] = Board::Position(0, 4);

	std::random_device randomDevice;
	std::mt19937 generator(randomDevice());
	std::shuffle(finalCards.begin(), finalCards.end(), generator);

	std::optional<PathCard> goldCard = PathCard(Enums::PathCardType::NSWEC, true);
	std::optional<PathCard> rockCard = PathCard(Enums::PathCardType::NSWEC, false);
	
	//initialize ends
	this->m_cards[finalCards[0].first*kWidth + finalCards[0].second] = goldCard;
	this->m_cards[finalCards[1].first*kWidth + finalCards[1].second] = rockCard;
	this->m_cards[finalCards[2].first*kWidth + finalCards[2].second] = rockCard;

	//initialize start 
	this->m_cards[8 * kWidth + 2] = std::optional<PathCard>(Enums::PathCardType::NSWEC);
}

bool Board::pathReachedGold(Board& board)
{
	std::queue < Board::Position > path;

	std::vector<Board::Position> visited;

	Board::Position startPosition(8, 2);
	path.push(startPosition);


	while (!path.empty())
	{
		auto front = path.front();
		if (board[front].value().isFinalGoldCard())
		{
			return true;
		}

		//west
		auto westPosition = Board::Position(front.first, front.second - 1);
		if (inBound(westPosition))
		{
			if (board[westPosition].has_value() &&
				board[front].value().getPathToWest() &&
				board[westPosition].value().getPathToEast() &&
				board[front].value().getPathToCenter() &&
				std::find(visited.begin(), visited.end(), westPosition) == visited.end())
			{
				path.push(westPosition);
				visited.push_back(westPosition);
			}
		}

		//south
		auto southPosition = Board::Position(front.first + 1, front.second);
		if (inBound(southPosition))
		{
			if (board[southPosition].has_value() &&
				board[front].value().getPathToSouth() &&
				board[front].value().getPathToCenter() &&
				board[southPosition].value().getPathToNorth() &&
				std::find(visited.begin(), visited.end(), southPosition) == visited.end())
			{
				path.push(startPosition);
				visited.push_back(southPosition);
			}
		}

		//east
		auto eastPosition = Board::Position(front.first, front.second + 1);
		if (inBound(eastPosition))
		{
			if (board[eastPosition].has_value() &&
				board[front].value().getPathToEast() &&
				board[front].value().getPathToCenter() &&
				board[eastPosition].value().getPathToWest() &&
				std::find(visited.begin(), visited.end(), eastPosition) == visited.end())
			{
				path.push(eastPosition);
				visited.push_back(eastPosition);
			}
		}

		//north
		auto northPosition = Board::Position(front.first - 1, front.second);
		if (inBound(northPosition))
		{
			if (board[northPosition] &&
				board[front].value().getPathToNorth() &&
				board[front].value().getPathToCenter() &&
				board[northPosition].value().getPathToSouth() &&
				std::find(visited.begin(), visited.end(), northPosition) == visited.end())
			{
				path.push(northPosition);
				visited.push_back(northPosition);
			}
		}
		path.pop();
	}
	return false;
}

bool Board::inBound(Board::Position position)
{
	if (position.first >= 0 && position.second >= 0)
	{
		if (position.first < kHeight && position.second < kWidth)
		{
			return true;
		}
	}
	return false;
}
