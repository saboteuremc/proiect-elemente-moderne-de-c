#include "stdafx.h"
#include "CppUnitTest.h"
#include "ActionCard.h"


using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace SaboteurTests
{
	TEST_CLASS(ActionCardsTest)
	{
	public:

		TEST_METHOD(revealDestination)
		{
			Board board;
			ActionCard mapCard;
			Player *player = new Player();
			Board::Position p(3, 3);
			Board::Position p1 = p;

			mapCard.executeAction(player, board, p);
			Assert::AreNotEqual(p.first, p1.first);
			Assert::AreNotEqual(p.second, p1.second);
		}

		TEST_METHOD(brokenToolCardConstructor)
		{
			ActionCard card(Enums::ActionCardType::BrokenCart);
			Assert::IsTrue(card.getType() == Enums::ActionCardType::BrokenCart);
		}


		// rewatch this <= ----------------------------------------TO REWATCH
		TEST_METHOD(brokenToolCardExecuteAction)
		{
			ActionCard card;
			card.setType(0);
		}


		TEST_METHOD(repairToolConstructor)
		{
			ActionCard card(Enums::ActionCardType::RepairLampAndCart);
			Assert::IsTrue(card.getType() == Enums::ActionCardType::RepairLampAndCart);
		}

		// rewatch this <= ----------------------------------------TO REWATCH
		TEST_METHOD(repairToolExecuteAction)
		{
			ActionCard card;
			card.setType(0);
		}

		TEST_METHOD(rockFallCardDestroyPath)
		{
			Action card;
			Board board;
			Board::Position position(7, 2);
			board[position] = Enums::PathCardType::NSWEC;
			Player* player = new Player();

			card.executeAction(player, board, position);
			Assert::IsTrue(board[Board::Position(7, 2)] == std::nullopt);
		}



	};
}